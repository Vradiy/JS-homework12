function createInputs() {

    const sizeInput = document.createElement('input');
    sizeInput.className = 'size-input';
    sizeInput.id = 'sizeInput';
    sizeInput.placeholder = 'Введите диаметр круга'
    document.body.appendChild(sizeInput);

    const colorInput = document.createElement('input');
    colorInput.className = 'color-input';
    colorInput.id = 'colorInput';
    colorInput.placeholder = 'Введите цвет круга'
    document.body.appendChild(colorInput);

    const drawCirlce = document.createElement('button');
    drawCirlce.innerHTML = 'Рисовать';
    drawCirlce.className = 'draw-button';
    drawCirlce.id = 'drawCircleBtn';
    document.body.appendChild(drawCirlce);

htmlButton.remove();


function circleDrawing() {

    const sizeInputValue = document.getElementById('sizeInput');
    const diameterValue = sizeInputValue.value;

    const colorInputValue = document.getElementById('colorInput');
    const colorValue = colorInputValue.value;

    let circleDiv = document.createElement('div');
    circleDiv.className = 'circle-div';
    document.body.appendChild(circleDiv);
    circleDiv.style.cssText = `background-color: ${colorValue};
                               width:            ${diameterValue}px;
                               height:           ${diameterValue}px`;

};
const drawCircleBtn = document.getElementById('drawCircleBtn');
drawCircleBtn.addEventListener('click',circleDrawing);

};
const htmlButton = document.getElementById('draw');
htmlButton.addEventListener('click',createInputs);

